# Project server implementation

Sails + CoffeeScript + Jade + Sass + Zurb Foundation.

## Status

Optimized configuration of [Sails.js](http://sailsjs.org/) with Rails-like asset pipeline. It should save a couple hours of configuration at a hackathon but has not yet been audited for production use.

* **Configuration**: good — optimized gruntfile with better asset handling
* **Development**: good — intuitive code layout and automatic loading
* **Test**: see [Sails.js](http://sailsjs.org/)
* **Production**: see [Sails.js](http://sailsjs.org/)

## Features

* Sails 0.9.16+
* Optimized Gruntfile for jade + sass + coffee
* Vendor directory for clean separation of app and vendor code
  * Modified Gruntfile to automatically compile and copy vendor files
* Automatic inclusion of js and template files
  * `assets/templates/*`
  * `assets/js/*.(js|coffee|)`
* Manual inclusion of vendor files
  * `vendor/*`
  * Allows for better control over vendor bloat
* Manual inclusion of stylesheets via sass @import
  * `assets/styles/app.scss`
  * Allows for more intuitive development and control than automatic inclusion
* Automatic support for common client-side templates: jade, ejs, dust, etc.
  * Files in assets/templates are compiled and combined into jst.js
  * See [consolidate.js](https://github.com/visionmedia/consolidate.js/) for list of supported templates
* Automatic support for JS and CoffeeScript


config/jade.js

```javascript
// config/jade.js
module.exports.config = function () {
  if (sails.config.environment === 'development') {
    // Pretty print output
    sails.express.app.locals.pretty = true;
  }
};
```

Load config/jade.js in config/bootstrap.js

```javascript
// Configure jade settings
require('./jade').config();
```

Add grunt-contrib-sass to Gruntfile.js

```javascript
// Gruntfile.js
// ...
grunt.loadTasks(depsPath + '/grunt-contrib-coffee/tasks');
grunt.loadNpmTasks('grunt-contrib-sass');
```

Modify [Gruntfile](https://github.com/starterkits/sails-starterkit/blob/master/Gruntfile.js) to support vendor dir, linking, etc. See [commit](https://github.com/starterkits/sails-starterkit/commit/8190716d5f088c886cc15354a1561c179d26c6ee).
