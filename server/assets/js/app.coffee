###
app.js

This file contains some conventional defaults for working with Socket.io + Sails.
It is designed to get you up and running fast, but is by no means anything special.

Feel free to change none, some, or ALL of this file to fit your needs!
###

io = window.io

# Simple log function to keep the example simple
log = (args...) ->
  console.log.apply console, args  if typeof console isnt 'undefined'

# As soon as this file is loaded, connect automatically
socket = io.connect()
log 'Connecting to Sails.js...'  if typeof console isnt 'undefined'

socket.on 'connect', socketConnected = ->
  onConnect()
  socket.on 'message', onMessage

onConnect = ->
  ###########################################################
  # Here's where you'll want to add any custom logic for
  # when the browser establishes its socket connection to
  # the Sails.js server.
  ###########################################################
  log "Socket is now connected"
  ###########################################################

onMessage = (message) ->
  ###########################################################
  # Replace the following with your own custom logic
  # to run when a new message arrives from the Sails.js
  # server.
  ###########################################################
  log "New comet message received :: ", message
  ###########################################################

# Expose connected `socket` instance globally so that it's easy
# to experiment with from the browser console while prototyping.
window.socket = socket

$(document).foundation();

Settings = {
  section:
    begin: '{'
    end:   '}'

  chord:
    begin: '['
    end:   ']'

  comment:
    begin: '#'
    end:   ''
}

#Helper functions
htmlDecode = (input) ->
  e = document.createElement 'div'
  e.innerHTML = input
  return e.childNodes.length is 0 ? '': e.childNodes[0].nodeValue

jQuery ($) ->
  lyrics  = $ 'textarea#lyrics'
  section = $ 'a[href=#section]'
  chord   = $ 'a[href=#chord]'
  comment = $ 'a[href=#comment]'
  meta    = $ 'a[href=#meta]'
  save    = $ 'a[href=#save]'
  saveNew = $ 'a[href=#save-new]'
  saveSlide= $ 'a[href=#save-slide]'

  #Handle the section ModalWindow and section insert
  section.click (event) ->
    event.preventDefault()
    #Setup symbolic names
    $root   = '#section-modal '
    sec_mod = $ $root
    insert  = $ $root + 'a[href=#insert]'
    value   = $ $root + 'input#value'

    #Open the Modal Window
    sec_mod.foundation 'reveal','open'

    #Clean the input
    insert.val ''
    tmp = lyrics.val()

    #Trigger INSERT action
    insert.click (event) ->
      event.preventDefault()
      lyrics.val tmp + Settings.section.begin + value.val() + Settings.section.end + '\n'
      lyrics[0].focus()
      sec_mod.foundation 'reveal','close'
      return 
      #insert.click
    return 
    #section.click

  #Handle the Chords ModalWindow and inserts
  chord.click (event) ->
    event.preventDefault()
    #Setup symbolic names
    $root   = '#chord-modal '
    chr_mod = $ $root
    insert  = $ $root + 'a[href=#insert]'
    value   = $ $root + 'input#value'

    #Open modal window
    chr_mod.foundation 'reveal', 'open'

    #Clear the input
    insert.val ''
    tmp = lyrics.val()

    #Trigger INSERT action
    insert.click (event) ->
      event.preventDefault()
      lyrics.val tmp + Settings.chord.begin + value.val() + Settings.chord.end
      lyrics[0].focus()
      chr_mod.foundation 'reveal','close'
      return 
      #insert.click
    return
    #chord.click

  #Handle the Comment ModalWindow and inserts
  comment.click (event) ->
    event.preventDefault()
    #Setup symbolic names
    $root   = '#comment-modal '
    cmt_mod = $ $root
    insert  = $ $root + 'a[href=#insert]'
    value   = $ $root + 'input#value'

    #Open modal window
    cmt_mod.foundation 'reveal', 'open'

    #Clear the input
    insert.val ''
    tmp = lyrics.val()

    #Trigger INSERT action
    insert.click (event) ->
      event.preventDefault()
      lyrics.val tmp + Settings.comment.begin + value.val() + Settings.comment.end + '\n'
      lyrics[0].focus()
      cmt_mod.foundation 'reveal','close'
      return 
      #insert.click
    return 
    #chord.click

  meta.click (event) ->
    event.preventDefault()
    #symbolic names
    $root = '#meta-modal '
    cmt_mod = $ $root
    insert  = $ $root + 'a[href=#insert]'

    #Open modal window
    cmt_mod.foundation 'reveal', 'open'

    insert.click (event) ->
      event.preventDefault()
      cmt_mod.foundation 'reveal','close'
      return 
      #insert.click
    return 
    #chord.click

  save.click (event) ->
    event.preventDefault()

    #symbolic names
    plaintext = lyrics.val()
    metadata=
      title: ($ '#name' ).val()

    console.log metadata.title

    socket.post '/editor/save', {type:'edit', raw: plaintext, meta: metadata}, (r) ->
      console.log r
      xml = $ '#xml'
      xml.val r.xml
      #htmlDecode r.xml
      return
    return

  saveNew.click (event) ->
    event.preventDefault()

    #symbolic names
    plaintext = lyrics.val()
    metadata=
      title: ($ '#name' ).val()

    console.log metadata.title

    socket.post '/editor/save', {type:'new', raw: plaintext, meta: metadata}, (r) ->
      console.log r
      #xml = $ '#xml'
      #xml.val r.xml
      #htmlDecode r.xml
      return
    return

  saveSlide.click (event) ->
    event.preventDefault()

    #symbolic names
    plaintext = ($ '#slide').val()
    metadata=
      title: ($ '#name' ).val()

    console.log metadata.title

    socket.post '/slide/save', {type:'new', slides: plaintext, name: metadata.title}, (r) ->
      console.log r
      #xml = $ '#xml'
      #xml.val r.xml
      #htmlDecode r.xml
      return
    return


#song() if typeof window.song isnt 'undefined'
