module.exports = {
    config: function () {
        if (sails.config.environment === 'development') {
            sails.express.app.locals.pretty = true; // Pretty print output
        }
    }
};