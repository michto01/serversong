#Module for creating, importing and exporting the songs
generator = require 'xmlbuilder'
xamel = require 'xamel'

#Song templete skeleton for xmlBuilder-js module
#Document Top level structure (without the verses)
_song = 
    song:
        '@xmlns'       : 'http://openlyrics.info/namespace/2009/song' # <OL> Namespace
        '@version'     : '0.8'                                        # <OL> Version
        '@createdIn'   : 'ServerSong@0.0.1~Aplha'                     # Origin Application
        '@modifiedIn'  : null                                         # Modification App
        '@modifiedDate': null                                         # Modification date

        properties:                                                   # List song properties
            titles:                                                   # Song titles (eg.: en, fi)
                title:                                                # Single title (Amazing Gr.)
                    '#text': ''
            authors:                                                  # Authors list
                author:                                               # Single author (J. Newton)
                    '#text': ''
            copyright:                                                # Ususally PD
                '#text': 'Public Domain'
            ccliNo:                                                   # Church Worship index no.
                '#text': ''
            releaseDate:                                              # Date song was released
                '#text': ''
            tempo:                                                    # Tempo descriptor
                '@type': 'text'                                       # Tempo type {bpm,text,...}
                '#text': 'moderate'                                   # Tempo's value
            key:                                                      # Song key
                '#text': 'C'
            verseOrder:                                               # Default song pres. order
                '#text': 'v1 v2 c'
            themes: ''                                                # TODO:  types of song
        lyrics: null                                                  # Array of _verse obj's

#Song templete: VERSE section
_verse = 
    verse:                                                            # Single verse struct
        '@name': ''                                                   # Verse name (use: order)
        lines: []

_lines=
    line:
        '#text': null

class Parser
    constructor: (@data, fn) ->
        @plaintext @data, (err,data) ->
            @verses data, (err,data) ->
                fn null, data  #fire callback
                return
            return
        return

    ##
    # plaintext arser for incoming text string
    ##
    # @data string   - Data to parse
    # @fn   callback - Return Array of parsed sections
    ##
    @plaintext: (data, fn) =>
        lines = data.split /\n/
        output = {}
        #Read single line
        for line in lines
            #Matches section marker ('{%name%}') ?
            if line.match /^{\w+\}$/
                key = line.substr 1, line.length - 2
                output[key] = ''
            else
                output[key] += line + '\n'

        fn null, output #fire callback
        return

    ##
    #  Versalize it
    ##
    # @data string   - Data to parse
    # @fn   callback - Return Array of parsed sections
    ##
    @verses: (data, fn) =>
        parsedVerses = []

        for name of data
            _tmpVerse = JSON.parse JSON.stringify _verse # Make template copy
            _tmpVerse.verse['@name'] = name              # Add verse name      

            @chords data[name], (err, _data) ->          # Check for chords
                data[name] = _data
                return

            @comments data[name], (err, _data) ->        # Check for commnets
                data[name] = _data
                return

            for line in data[name].split '\n'
                continue if line is '' #skip empty lines
                _tmp = JSON.parse JSON.stringify _lines
                _tmp.line['#text'] = line
                _tmpVerse.verse.lines.push _tmp 
            parsedVerses.push _tmpVerse

        fn null, parsedVerses #fire callback
        return

    ##
    #  Detect and parse chords
    ##
    # @data line               - Data to parse
    # @fn   callback(err,data) - Return Array of parsed sections
    ##
    @chords: (line,fn) =>
        fn null, line.replace /\[([a-zA-Z0-9#]*)\]/g, '<'+'chord name='+'$1'+'/>'
        return

    ##
    # comment parser for incoming text string
    ##
    # @data string   - Data to parse
    # @fn   callback - Return Array of parsed sections
    ##
    @comments: (line, fn) =>
        #Replace commnet marker ('#%comment%\n') with <comment /> tag pair
        fn null, line.replace /^#(.*)/, '<'+'comment'+'>'+'$1'+'</'+'comment'+'>'
        return


exports.OpenLyrics= 
    parse: (xml, fn) ->
        metadata = null
        lyrics = null
        descriptor = null

        xamel.parse xml, (err, xml) ->
            return fn(err, null) if err isnt null
            lyrics = xml.$ 'song/lyrics/*'
            metadata = {}
            metadata.title       = xml.$ 'song/properties/titles/title/text()'
            metadata.author      = xml.$ 'song/properties/authors/author/text()'
            metadata.copyright   = xml.$ 'song/properties/copyright/text()'
            metadata.ccliNo      = xml.$ 'song/properties/ccliNo/text()'
            metadata.releaseDate = xml.$ 'song/properties/releaseDate/text()'
            metadata.tempo       = {}
            metadata.tempo.type  = xml.$ 'tempo/attr(type)'
            metadata.tempo.value = xml.$ 'tempo/text()'
            metadata.key         = xml.$ 'song/properties/key/text()'
            metadata.verseOrder  = xml.$ 'song/properties/verseOrder/text()'
            metadata.themes      = xml.$ 'song/properties/themes/theme/text(true)'

            descriptor = xml.$ 'song/'

        fn null, {
            lyrics:     JSON.stringify lyrics
            metadata:   JSON.stringify metadata
            descriptor: JSON.stringify descriptor
        }

    createXML: (chunks, fn) ->
        # Assign incoming parameters to variables
        plainText = chunks.raw
        metaData  = chunks.meta

        #Take care of empty inputs
        return res.json {msg: 'empty'} if plainText is null or undefined

        #Parse text for SCTIONS
        Parser.plaintext plainText, (err, data) ->
            console.log 'Parser Error' if err isnt null
            Parser.verses data, (err, data) ->
                console.log 'Parser Error' if err isnt null
                #Make xml
                objXml = JSON.parse JSON.stringify _song #local template copy
                
                #objXml.song.titles.title["#text"] = metaData.title
                

                objXml.song.lyrics = data

                xml = generator.create objXml
                fn null, xml
                #res.json {msg: xml.toString()}
                return

    # Presenter
    presenter: (xml, fn) ->
        lyrics = null

        xamel.parse xml, (err, xml) ->
            return fn(err, null) if err isnt null
            lyrics = xml.$ 'song/lyrics/*'

        fn null, lyrics

