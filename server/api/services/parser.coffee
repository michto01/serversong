# Currently not in use

module.exports =
	parser: (data, fn) ->
		lines = data.split /\n/
		output = {}

		for line in lines
			if line.match /^{\w+\}$/
				key = line.substr 1, line.length - 2
				output[key] = ''
			else
				output[key] += line

		fn null, output
		#return output