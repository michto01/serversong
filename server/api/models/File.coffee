#UUIDGenerator  = require 'node-uuid'

sanitizeName = (name) ->
  #name = name.replace(/ /g, '-')
  name = name.replace /[^A-Za-z0-9-_\.]/g, ''
  name = name.replace /\.+/g, '.'
  name = name.replace /-+/g, '-'
  name = name.replace /_+/g, '_'
  name

module.exports =

  attributes:
    uuid:
      type     : 'string'
      required : true
      unique   : true

    name:
      type     : 'string'
      required : true

    storage:
      type     : 'string'
      required : true

    extension:
      type     : 'string'
      required : true

    #size: ?Is it needed -> not now

    toJSON: ->
      obj = this.toObject()
      obj

    #beforeCreate: (file, cb) ->
    #  file.uuid    = UUIDGenerator.v1()
    #  cb(null, user)

