module.exports =
  attributes:
    name:
      type: "string"
      required: true

    content:
      type: "string"
      required: true

    toJSON: ->
      obj = this.toObject()
      obj