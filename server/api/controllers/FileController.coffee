# UUIDGenerator  = require 'node-uuid' -> UUIDGenerator.v1()
uuid = require 'shortid'
fs   = require 'fs'

module.exports =
    
    upload: (req, res) ->
        #console.log req.files.upload
        id = uuid.generate()

        fs.readFile req.files.upload.path, (err, data) ->
            file = req.files.upload.name.split '.'

            # Create file descriptor
            metadata = {
                    uuid      : id
                    name      : file[0]
                    extension : file[1]
                    storage   : 'assets/files/' + id + '.' + file[1]
            }

            fs.writeFile metadata.storage, data, (err) ->
                res.view {err: err} if err

                # Save file in database
                File.create(metadata).done (err, user) ->
                    res.send {success:false,msg: "File upload failded"} if err
                    #The User was created successfully!
                    res.send {success:true,msg: "File uploaded"}


    download: (req,res) ->
        File.findOne({uuid: req.param('id')}).done (err, data) ->
            return res.json {success: false, msg: "No file found"} if err isnt null
            res.sendfile data.storage


    uploader: (req, res) ->
        res.view {}

    _config: {}

