module.exports =

    create: (req, res) ->
        return res.view {}

    save: (req, res) ->
        name=   req.param('name')
        slides= req.param('slides')

        Slide.create({name:name,content:slides}).done (err, data) ->
        	req.socket.emit('action:save:ack',{success:false,msg:"Slide creation failed!"}) if err isnt null
        	req.socket.emit('action:save:ack',{success:true,msg:"Slide Created!"})
        	res.json {success: err ? true:false}

    _config: {}
