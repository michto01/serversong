uuid = require 'shortid'
fs = require 'fs'

module.exports =
    index: (req, res) ->
        res.view {}

    list: (req, res) ->
        File.find().done (err, data) ->
            return res.json {success:false, msg: "File error (no files)"} if err isnt null

            #Files are in DB send the list
            res.view {sucess: true, list: data}

    open: (req, res) ->
        File.findOne({uuid: req.param('id')}).done (err, data) ->
            return res.json {sucess: false, msg: "File error (not found"} if err isnt null

            #Founded open:
            fs.readFile data.storage, 'utf8', (err, file) ->
                output = '' 
                return res.json {success:false, msg: "File error (cannot open file)"} if err isnt null
                #console.log file
                FileService.OpenLyrics.parse file, (err, obj) ->

                    # Create OpenSong Formated data from XML
                    for item in JSON.parse obj.lyrics
                        output  += '{' + item["attrs"]["name"] + '}\n'
                        for line in item["children"][0]["children"]
                            output += line["text"] + '\n'
                        output += '\n'

                    res.view {raw: output, desc: obj.descriptor, meta: JSON.parse obj.metadata}

    save: (req,res) ->
        chunks=
            raw:  req.param('raw')
            meta: req.param('meta')

        type= req.param 'type'

        if type is 'edit'
            FileService.OpenLyrics.createXML chunks, (err, xml) ->
                res.json {
                    sucess: true
                    xml:    xml.toString()
                }

        if type is 'new'
            FileService.OpenLyrics.createXML chunks, (err, xml) ->
                console.log 'createdXML: ' + xml.toString()

                filename= ".tmp/" + chunks.meta.title + '.xml'
                data = xml.toString()

                fs.writeFile filename, data, (err, r) ->
                    return console.log "File error" + err if err isnt null
                    
                    console.log "Here"

                    id = uuid.generate()
                    fs.readFile filename, (err, data) ->

                        # Create file descriptor
                        metadata = {
                            uuid      : id
                            name      : chunks.meta.title
                            extension : 'xml'
                            storage   : 'assets/files/' + id + '.' + '.xml'
                        }

                        fs.writeFile metadata.storage, data, (err) ->
                            res.view {err: err} if err isnt null

                            # Save file in database
                            File.create(metadata).done (err, user) ->
                                res.send {success:false,msg: "File upload failded"} if err
                                #The Song was created successfully!
                                res.send {success:true,msg: "File uploaded"}
           
        else
            res.json {sucess:false, msg: 'unknown save operation type', type: req.param 'type'}

    new: (req, res) ->
        res.view {}

    import: (req, res) ->
        res.json {sucess: false, msg: "Import Failed"}

    export: (req, res) ->
        res.json {sucess: true, msg: 'Export completed'}

    _config: {}
