fs = require 'fs'

module.exports =

    view: (req, res) ->
        res.view {slides: null}
        #return res.json {hello: req.param('id')}

    control: (req, res) ->
        packet = {}
        File.find().done (err, data) ->
            packet.list = data
        Slide.find().done (err, data) ->
            packet.slides = data
        res.view packet

    api: (req, res) ->
        presentations = {}
        # Change slide
        req.socket.on "slidechanged", (data) ->
            console.log "slidechanged"
            req.socket.broadcast.emit "slidechanged", data
            return

        # Remote slide controller
        req.socket.on 'commnad', (cmd) ->
            console.log 'Recieved commnad: ' + JSON.stringify cmd

            if presentations[cmd.id]
                current = presentations[cmd.id]

                current.indexv-- if cmd.txt is 'up'
                current.indexv++ if cmd.txt is 'down'
                current.indexv is 0 if current.indexv < 0

                current.indexh-- if cmd.txt is 'left'
                current.indexh++ if cmd.txt is 'right'
                current.indexh is 0 if current.indexh < 0

                presentations[cmd.id] = current

                req.socket.broadcast.emit 'updatedata', current

        # Request presentation data
        req.socket.on 'request', (data) ->
            console.log "Requested new presentation: " + data.id
            File.findOne({uuid: data.id}).done (err, presentation) ->
                return console.log {success:false, msg:'no PresenationID found'} if err isnt null

                # Else: ( #Founded open: )
                
                fs.readFile presentation.storage, 'utf8', (err, file) ->
                    output = ''
                    return console.log {success:false, msg: "File error (cannot open file)"} if err isnt null
                    
                    #console.log file
                    FileService.OpenLyrics.parse file, (err, obj) ->
                        # Create OpenSong Formated data from XML
                        for item in JSON.parse obj.lyrics
                            output  += '<section id="' + item["attrs"]["name"] + '">'
                            for line in item["children"][0]["children"]
                                output += '<h3>' + line["text"] + '</h3>'
                            output += '</section>'

                    console.log 'Sending data' + JSON.stringify(output)
                    req.socket.broadcast.emit 'initdata', output

        res.send {sucess: true, msg: "Connected to presentation backend"}

        # Request slide data
        req.socket.on 'request-slide', (data) ->
            console.log "Requested new slides: " + data.id
            Slide.findOne(data.id).done (err, presentation) ->
                return console.log {success:false, msg:'no SlideID found'} if err isnt null

                # Else: ( #Founded open: )
                console.log 'Sending SLIDE data' + presentation.content
                req.socket.broadcast.emit 'initdata', presentation.content

        res.send {sucess: true, msg: "Connected to presentation:slide backend"}



    _config: {}

