#Module for creating, importing and exporting the songs
generator = require 'xmlbuilder'
exml      = require 'exml'

#Song templete skeleton for xmlBuilder-js module
#Document Top level structure (without the verses)
_song = 
    song:
        '@xmlns'       : 'http://openlyrics.info/namespace/2009/song' # <OL> Namespace
        '@version'     : '0.8'                                        # <OL> Version
        '@createdIn'   : 'ServerSong@0.0.1~Aplha'                     # Origin Application
        '@modifiedIn'  : null                                         # Modification App
        '@modifiedDate': null                                         # Modification date

        properties:                                                   # List song properties
            titles:                                                   # Song titles (eg.: en, fi)
                title:                                                # Single title (Amazing Gr.)
                    '#text': ''
            authors:                                                  # Authors list
                author:                                               # Single author (J. Newton)
                    '#text': ''
            copyright:                                                # Ususally PD
                '#text': 'Public Domain'
            ccliNo:                                                   # Church Worship index no.
                '#text': ''
            releaseDate:                                              # Date song was released
                '#text': ''
            tempo:                                                    # Tempo descriptor
                '@type': 'text'                                       # Tempo type {bpm,text,...}
                '#text': 'moderate'                                   # Tempo's value
            key:                                                      # Song key
                '#text': 'C'
            verseOrder:                                               # Default song pres. order
                '#text': 'v1 v2 c'
            themes: ''                                                # TODO:  types of song
        lyrics: null                                                  # Array of _verse obj's

#Song templete: VERSE section
_verse = 
    verse:                                                            # Single verse struct
        '@name': ''                                                   # Verse name (use: order)
        lines:                                                        # Actual lines
            '#text': null


#Parsers for incoming text stream
class Parser
    constructor: (@data, fn) ->
        @plaintext @data, (err,data) ->
            @verses data, (err,data) ->
                fn null, data  #fire callback
                return
            return
        return

    ##
    # plaintext arser for incoming text string
    ##
    # @data string   - Data to parse
    # @fn   callback - Return Array of parsed sections
    ##
    @plaintext: (data, fn) =>
        lines = data.split /\n/
        output = {}
        #Read single line
        for line in lines
            #Matches section marker ('{%name%}') ?
            if line.match /^{\w+\}$/
                key = line.substr 1, line.length - 2
                output[key] = ''
            else
                output[key] += line + '\n'

        fn null, output #fire callback
        return

    ##
    #  Versalize it
    ##
    # @data string   - Data to parse
    # @fn   callback - Return Array of parsed sections
    ##
    @verses: (data, fn) =>
        parsedVerses = []

        for name of data
            _tmpVerse = JSON.parse JSON.stringify _verse # Make template copy
            _tmpVerse.verse['@name'] = name              # Add verse name      

            @chords data[name], (err, _data) ->          # Check for chords
                data[name] = _data
                return

            @comments data[name], (err, _data) ->        # Check for commnets
                data[name] = _data
                return

            _tmpVerse.verse.lines['#text'] = data[name].replace '\n', '<'+'br'+'/>'
            parsedVerses.push _tmpVerse

        fn null, parsedVerses #fire callback
        return

    ##
    #  Detect and parse chords
    ##
    # @data line               - Data to parse
    # @fn   callback(err,data) - Return Array of parsed sections
    ##
    @chords: (line,fn) =>
        fn null, line.replace /\[([a-zA-Z0-9#]*)\]/g, '<'+'chord name='+'$1'+'/>'
        return

    ##
    # comment parser for incoming text string
    ##
    # @data string   - Data to parse
    # @fn   callback - Return Array of parsed sections
    ##
    @comments: (line, fn) =>
        #Replace commnet marker ('#%comment%\n') with <comment /> tag pair
        fn null, line.replace /^#(.*)/, '<'+'comment'+'>'+'$1'+'</'+'comment'+'>'
        return


# Module exposition to NodeJS
module.exports =
    index: (req, res) ->
        res.view {}

    save: (req, res) ->
        # Assign incoming parameters to variables
        plainText = req.param 'content'
        metaData  = req.param 'meta'

        #Take care of empty inputs
        res.json {msg: 'empty'} if plainText is null or undefined

        #Parse text for SCTIONS
        Parser.plaintext plainText, (err, data) ->
            console.log 'Parser Error' if err isnt null
            Parser.verses data, (err, data) ->
                console.log 'Parser Error' if err isnt null
                #Make xml
                objXml = JSON.parse JSON.stringify _song #local template copy
                objXml.song.lyrics = data

                xml = generator.create objXml
                res.json {msg: xml.toString()}
                return

    template: (req,res) ->
        res.json {
            type: 'template'
            content: _song
        }

    presentation: (req,res) ->
        ans = []
        parser = new exml.Parser()
        parser.on 'song', () ->
            parser.on 'lyrics', () ->
                parser.on 'verse', '$content', (node) ->
                    ans.push(node)

        parser.write '<song><lyrics><verse name="igor"><lines>alkd lkajdf laksdjf lkafj lkasjf lkajf <br /> jkfjfkjff</lines></verse><verse name="igor2"><lines>aluhdfu555522fj lkasjf lkajf <br /> jkfjfkjff</lines></verse></lyrics></song>'
        parser.end()

        res.view {mama: '<h3>mama</h3>', tpl: ans}

    edit: (req, res) ->
        res.json {msg: 'Data edited'}

    _config: {}